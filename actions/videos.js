import * as helpers from "../helpers";

export const bookmarkVideo = video => ({
    type: "BOOKMARK_VIDEO",
    video
});

export function fetchData() {
    const url = "http://app.rguc.co.uk/?rest_route=/wp/v2/posts";

    return dispatch => {
        fetch(url)
            .then(response => response.json())
            .then(data => {
                if (data) {
                    dispatch(fetchSuccessVideos(data));
                } else {
                    dispatch(fetchErrorVideos(data));
                }
            })
            .catch(error => {
           
            });
    };
}

export const filteredVideo = data => ({
    type: "FILTERED_VIDEOS",
    data
});

export const bookmarkSuccess = bookMarkedVideos => ({
    type: "BOOKMARK_VIDEO",
    bookMarkedVideos
});

export const fetchSuccessVideos = data => ({
    type: "VIDEOS_SUCCESS",
    data
});

export const submitAnswer = data => ({
    type: "SUBMIT_ANSWER",
    data
});

export const nextQuestion = data => ({
    type: "NEXT_QUESTION",
    data
});

export const resetQuiz = id => ({
    type: "RESET_QUIZ",
    id
});
export const initSearch = () => ({
    type: "INIT_SEARCH"
});

export const fetchAboutSuccess = data => ({
    type: "ABOUT_US_SUCCESS",
    data
});
