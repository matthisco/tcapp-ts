const images = {
	"administrating-vaccinations": {
		imgName: "administrating-vaccinations",
		uri: require("../assets/images/administrating-vaccinations.png")
	},
	'blood-icon': {
		imgName: "blood-icon",
		uri: require("../assets/images/blood-icon.png")
	},
	catheter: {
		imgName: "catheter-icon",
		uri: require("../assets/images/catheter-icon.png")
	},
	"female-urinery-catheterisation": {
		imgName: "female-urinery-catheterisation",
		uri: require("../assets/images/female-urinery-catheterisation.png")
	},
	injection: {
		imgName: "injection-icon",
		uri: require("../assets/images/injection-icon.png")
	},
	"intramuscular-injections": {
		imgName: "intramuscular-injections",
		uri: require("../assets/images/intramuscular-injections.png")
	},
	"intravenous-cannulation": {
		imgName: "intravenous-cannulation",
		uri: require("../assets/images/intravenous-cannulation.png")
	},
	"local-anaesthetic-infiltration": {
		imgName: "local-anaesthetic-infiltration",
		uri: require("../assets/images/local-anaesthetic-infiltration.png")
	},
	"male-urinary-catheterisation": {
		imgName: "male-urinary-catheterisation",
		uri: require("../assets/images/male-urinary-catheterisation.png")
	},
	"managing-an-ecg-monitor": {
		imgName: "managing-an-ecg-monitor",
		uri: require("../assets/images/managing-an-ecg-monitor.png")
	},
	"manual-handling": {
		imgName: "manual-handling",
		uri: require("../assets/images/manual-handling.png")
	},
	"measuring-blood-glucose": {
		imgName: "measuring-blood-glucose",
		uri: require("../assets/images/measuring-blood-glucose.png")
	},
	"measuring-blood-pressure": {
		imgName: "measuring-blood-pressure",
		uri: require("../assets/images/measuring-blood-pressure.png")
	},
	"managing-an-ecg-monitor": {
		imgName: "managing-an-ecg-monitor",
		uri: require("../assets/images/managing-an-ecg-monitor.png")
	},
	"measuring-peak-expiratory-flow-rate": {
		imgName: "measuring-peak-expiratory-flow-rate",
		uri: require("../assets/images/measuring-peak-expiratory-flow-rate.png")
	},
	"nebuliser-therapy": {
		imgName: "nebuliser-therapy",
		uri: require("../assets/images/nebuliser-therapy.png")
	},
	"nose-and-groin-swabs": {
		imgName: "nose-and-groin-swabs",
		uri: require("../assets/images/nose-and-groin-swabs.png")
	},
	"managing-an-ecg-monitor": {
		imgName: "managing-an-ecg-monitor",
		uri: require("../assets/images/managing-an-ecg-monitor.png")
	},
	"nosogastric-tube-insertion": {
		imgName: "nosogastric-tube-insertion",
		uri: require("../assets/images/nosogastric-tube-insertion.png")
	},
	"nutritional-assessment": {
		imgName: "nutritional-assessment",
		uri: require("../assets/images/nutritional-assessment.png")
	},
	"obtaining-written-consent": {
		imgName: "obtaining-written-consent",
		uri: require("../assets/images/obtaining-written-consent.png")
	},
	"oxygen-therapy": {
		imgName: "oxygen-therapy",
		uri: require("../assets/images/oxygen-therapy.png")
	},
	"pregnancy-testing": {
		imgName: "pregnancy-testing",
		uri: require("../assets/images/pregnancy-testing.png")
	},
	"preparing-medications": {
		imgName: "preparing-medications",
		uri: require("../assets/images/preparing-medications.png")
	},
	"radial-arterial-blood-gas": {
		imgName: "radial-arterial-blood-gas",
		uri: require("../assets/images/radial-arterial-blood-gas.png")
	},
	"slide-sheets": {
		imgName: "slide-sheets",
		uri: require("../assets/images/slide-sheets.png")
	},
	"standing-a-patient": {
		imgName: "standing-a-patient",
		uri: require("../assets/images/standing-a-patient.png")
	},
	"subcutaneous-injections": {
		imgName: "subcutaneous-injections",
		uri: require("../assets/images/subcutaneous-injections.png")
	},
	"surgical-scrubbing-up": {
		imgName: "surgical-scrubbing-up",
		uri: require("../assets/images/surgical-scrubbing-up.png")
	},
	suturing: {
		imgName: "suturing",
		uri: require("../assets/images/suturing.png")
	},
	"taking-blood-cultures": {
		imgName: "taking-blood-cultures",
		uri: require("../assets/images/taking-blood-cultures.png")
	},
	"obtaining-written-consent": {
		imgName: "obtaining-written-consent",
		uri: require("../assets/images/obtaining-written-consent.png")
	},
	"temperature-measurement": {
		imgName: "temperature-measurement",
		uri: require("../assets/images/temperature-measurement.png")
	},
	urinalysis: {
		imgName: "urinalysis",
		uri: require("../assets/images/urinalysis.png")
	},
	"transcutaneous-oxygen": {
		imgName: "transcutaneous-oxygen",
		uri: require("../assets/images/transcutaneous-oxygen.png")
	},
	"using-hoist": {
		imgName: "using-hoist",
		uri: require("../assets/images/using-hoist.png")
	},
	venepuncture: {
		imgName: "venepuncture",
		uri: require("../assets/images/venepuncture.png")
	},
	"transcutaneous-oxygen": {
		imgName: "transcutaneous-oxygen",
		uri: require("../assets/images/transcutaneous-oxygen.png")
	},
	"standing-a-patient": {
		imgName: "transcutaneous-oxygen",
		uri: require("../assets/images/standing-a-patient.png")
	}
};

export { images };
