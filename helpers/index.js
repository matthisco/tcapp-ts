export const currentDate = new Date().toISOString().split("T")[0];

export const currentDateString = new Date().toISOString();

export function formatDate(start) {
	const date = new Date(start);

	return date.toISOString().split("T")[0];
}

export function timeToString(timestamp, index = 0) {
	if (typeof timestamp === "string") return timestamp;

	const time = timestamp + index * 24 * 60 * 60 * 1000;
	const date = new Date(time);

	return date.toISOString().split("T")[0];
}

export function checkDate(timestamp) {
	const date = new Date(timestamp);
	return isNaN(date);
}

export function trimText(text, length) {
	var yourString = text; //replace with your string.
	var maxLength = length || text.length; // maximum number of characters to extract

	//trim the string to the maximum length
	var trimmedString = yourString.substr(0, maxLength);

	// remove p tags
	trimmedString = trimmedString.replace(/(<p[^>]+?>|<p>|<\/p>)/gim, "");

	// remove returns
	trimmedString = trimmedString.replace(/\n/gi, "");

	//re-trim if we are in the middle of a word
	trimmedString = trimmedString.substr(
		0,
		Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))
	);

	return trimmedString;
}

export function getVideoId(url) {
	let videoId = url.split("v=")[1];
	let ampersandPosition = videoId.indexOf("&");
	if (ampersandPosition != -1) {
		videoId = videoId.substring(0, ampersandPosition);
	}
	return videoId;
}
