import React, { Component } from "react";
import {
  Platform,
  StatusBar,
  StyleSheet,
  View,
  Image,
  ImageBackground,
  ActivityIndicator
} from "react-native";
import NavigationService from "./navigation/NavigationService";
import { connect } from "react-redux";
import SplashScreen from 'react-native-splash-screen'
import { store, persistor } from "./config/configureStore";
import { PersistGate } from "redux-persist/integration/react";
import AppNavigator from "./navigation/AppNavigator";

class App extends React.Component {
  componentDidMount() {
    SplashScreen.hide();
  }
  render() {
    const { videos } = this.props;

    return (
      <ImageBackground
        source={require("./assets/images/TC_background.jpg")}
        style={styles.container}
      >
        <PersistGate loading={null} persistor={persistor}>
          <AppNavigator
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </PersistGate>
      </ImageBackground>
    );
  }
}

const mapStateToProps = state => {
  return {
    videos: state.videos
  };
};

export default connect(mapStateToProps)(App);

var styles = StyleSheet.create({
  container: {
    flex: 1
  },
  backgroundImage: {
    flex: 1,
    backgroundColor: "black"
  }
});
