import React, { Component } from "react";
import {
	Image,
	View,
	Text,
	Modal,
	Button,
	TouchableOpacity,
	AsyncStorage,
	StyleSheet,
	Platform,
	Alert,
	TouchableHighlight
} from "react-native";
import {
	createDrawerNavigator,
	createStackNavigator,
	createAppContainer
} from "react-navigation";

import HomeScreen from "../screens/HomeScreen";
import AboutScreen from "../screens/AboutScreen";
import MyResultsScreen from "../screens/MyResultsScreen";
import BookmarkedVideosScreen from "../screens/BookmarkedVideosScreen";
import TestYourselfScreen from "../screens/TestYourselfScreen";
import VideoEpisodesScreen from "../screens/VideoEpisodesScreen";
import VideoPlayerScreen from "../screens/VideoPlayerScreen";
import BasicHeader from "./BasicHeader";
import DrawerHeader from "./DrawerHeader";
const config = {
	contentOptions: {
		activeTintColor: "#e91e63",
		itemStyle: {
			flexDirection: "row-reverse"
		}
	},
	drawerWidth: 300,
	drawerPosition: "right",
	transparentCard: true,
	cardStyle: {
		backgroundColor: "transparent",
		opacity: 1
	},
	transitionConfig: () => ({
		containerStyle: {
			backgroundColor: "transparent"
		}
	})
};

const withHeader = (
	screen: Function,
	routeName: string,
	Header
): StackNavigator =>
	createStackNavigator(
		{
			[routeName]: {
				screen,
				navigationOptions: ({ routeName, props }) => ({
					header: props => <Header {...props} />
				})
			}
		},
		{
			initialRoute: "Home",
			transparentCard: true,
			cardStyle: {
				backgroundColor: "transparent",
				opacity: 1
			},
			transitionConfig: () => ({
				containerStyle: {
					backgroundColor: "transparent"
				}
			})
		}
	);

const routes = {
	VideoEpisodes: {
		screen: withHeader(VideoEpisodesScreen, "Video Episodes", DrawerHeader)
	},
	TestYourself: {
		screen: withHeader(TestYourselfScreen, "Test Yourself", DrawerHeader)
	},
	MyResults: {
		screen: withHeader(MyResultsScreen, "My Results", DrawerHeader)
	},
	BookmarkedVideos: {
		screen: withHeader(
			BookmarkedVideosScreen,
			"Bookmarked Videos",
			DrawerHeader
		)
	},
	// Search: {
	// 	screen: withHeader(SearchScreen, "Search", DrawerHeader)
	// },
	About: {
		screen: withHeader(AboutScreen, "About", DrawerHeader)
	}
};

const NestedDrawer = createDrawerNavigator(routes, config);

const MainStack = createStackNavigator(
	{
		Home: {
			screen: HomeScreen,
			navigationOptions: ({ props }) => ({
				header: props => <BasicHeader {...props} />
			})
		},
		Drawer: {
			screen: NestedDrawer,
			navigationOptions: ({ props }) => ({
				header: () => null
			})
		},
		VideoPlayer: {
			screen: VideoPlayerScreen,
			navigationOptions: ({ props }) => ({
				header: props => <BasicHeader {...props} />
			})
		}
	},
	{
		initialRoute: "Home",
		transparentCard: true,
		cardStyle: {
			backgroundColor: "transparent",
			opacity: 1
		},
		transitionConfig: () => ({
			containerStyle: {
				backgroundColor: "transparent"
			}
		})
	}
);

export default createAppContainer(MainStack);
