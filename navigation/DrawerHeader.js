import React, { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
    TouchableHighlight,
    Image,
    FlatList,
    StyleSheet,
    StatusBar,
    Platform
} from "react-native";
import Icon from "react-native-vector-icons/dist/FontAwesome";
import { connect } from "react-redux";
import {
    StackActions,
    NavigationActions,
    DrawerNavigator
} from "react-navigation";
import { ListItem, SearchBar } from "react-native-elements";
import { filteredVideo, initSearch } from "../actions/videos";
import StyledText from "../components/StyledText";
class DrawerHeader extends Component {
    constructor(props) {
        super(props);
        this.state = { term: "", videos: [] };
        this.onSubmitEdit = this.onSubmitEdit.bind(this);
    }
    componentDidMount() {
        initSearch();
    }
    SearchFilterFunction(term) {
        const { filteredVideo } = this.props;
        filteredVideo(term);
    }

    onSubmitEdit() {
        const {
            navigation: { navigate },
            initSearch
        } = this.props;

        navigate("VideoEpisodes");
    }

    onPress(id) {
        const {
            navigation,
            initSearch,
            navigation: { dispatch }
        } = this.props;
        initSearch();

        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({
                    routeName: "VideoPlayer",
                    params: { id }
                })
            ]
        });
        dispatch(resetAction);
    }

    render() {
        const {
            navigation,
            videos,
            search: { term },
            scene: {
                route: { routeName: title }
            }
        } = this.props;

        return (
            <View>
                <View style={styles.container}>
                    <View style={{ paddingLeft: 5 }}>
                        <StyledText text={title} />
                    </View>

                    <View style={{ justifyContent: "flex-end" }}>
                        <Image
                            source={require("../assets/images/tc-logo.png")}
                            style={{ width: 120, height: 40, marginRight: 5 }}
                        />
                        <TouchableHighlight
                            style={{
                                width: 32,
                                marginLeft: 10,
                                marginTop: 10,
                                marginRight: 5,
                                alignSelf: "flex-end"
                            }}
                            onPress={() => {
                                this.props.navigation.openDrawer();
                            }}
                        >
                            <Image
                                style={{
                                    width: 32,
                                    height: 25,
                                    margin: 3
                                }}
                                source={require("../assets/images/menu-icon.png")}
                            />
                        </TouchableHighlight>
                    </View>
                </View>
                <SearchBar
                    round
                    containerStyle={{
                        borderTopWidth: 0,
                        borderBottomWidth: 0
                    }}
                    inputStyle
                    inputContainerStyle={{
                        borderRadius: 5
                    }}
                    onSubmitEditing={this.onSubmitEdit}
                    searchIcon={{ size: 18 }}
                    onChangeText={text => this.SearchFilterFunction(text)}
                    onClear={text => this.SearchFilterFunction("")}
                    placeholder="Type Here..."
                    value={term}
                />
                {title !== "Video Episodes" ? (
                    <FlatList
                        data={this.props.search.videos}
                        renderItem={({ item }) => (
                            <ListItem
                                title={item.title}
                                onPress={() => this.onPress(item.id)}
                            />
                        )}
                        contentContainerStyle={{ overflow: "hidden" }}
                        keyExtractor={item => item.id.toString()}
                        ItemSeparatorComponent={this.renderSeparator}
                    />
                ) : null}
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    filteredVideo: data => dispatch(filteredVideo(data)),
    initSearch: () => dispatch(initSearch())
});

const mapStateToProps = state => {
    return {
        videos: state.tcApp.videos,
        search: state.tcApp.search
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DrawerHeader);

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingTop: Platform.OS === "ios" ? 20 : StatusBar.currentHeight
    },
    title: {
        color: "#fff",
        fontSize: 18
    }
});
