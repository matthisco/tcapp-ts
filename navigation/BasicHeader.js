import React, { Component } from "react";

import { connect } from "react-redux";
import {
  Image,
  View,
  Text,
  Modal,
  Button,
  TouchableOpacity,
  AsyncStorage,
  StyleSheet,
  Platform,
  Alert,
  StatusBar,
  TouchableHighlight
} from "react-native";

class BasicHeader extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require("../assets/images/tc-logo.png")}
          style={{ width: 120, height: 40 }}
        />
        <Image
          source={require("../assets/images/connect-logo.png")}
          style={{ width: 50, height: 60 }}
        />
      </View>
    );
  }
}

export default BasicHeader;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingLeft: 20,
    paddingRight: 30,
    paddingTop: Platform.OS === "ios" ? 20 : StatusBar.currentHeight
  },

  text: {
    color: "#fff",
    fontSize: 14
  }
});
