import { StackActions, NavigationActions } from "react-navigation";

function goBack(sceneKey) {
	NavigationActions.back({ key: sceneKey });
}

let _navigator;

function setTopLevelNavigator(navigatorRef) {
	_navigator = navigatorRef;
}

function navigate(routeName, params) {
	// _navigator.dispatch(
	// 	NavigationActions.navigate({
	// 		routeName,
	// 		params
	// 	})
	// );

	const pushAction = StackActions.push({
		routeName,
		params
	});

	_navigator.dispatch(pushAction);
}

const pushIt = (routeName, params) => StackActions.push({ routeName, params });

function getParams() {
	const {
		nav: { index: navIndex = "", routes: navRoutes = "" } = {},
		routes = "",
		index = ""
	} = _navigator.state;

	const params =
		navRoutes[navIndex].params ||
		navRoutes[navIndex].routes[navRoutes[navIndex].index].params;
	console.log(params);
	return params;
}

export default {
	navigate,
	pushIt,
	goBack,
	setTopLevelNavigator,
	getParams
};
