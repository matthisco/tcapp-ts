import React from "react";
import { View } from "react-native";
import { NavigationScreenProp } from "react-navigation";

export default class OverlayScreen extends React.Component {
    render() {
        const content = this.props.navigation.getParam("content");
        return (
            <View style={{ flex: 1, backgroundColor: "transparent" }}>
                {content()}
            </View>
        );
    }
}
