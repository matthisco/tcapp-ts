import { trimText } from "../helpers";

export function tcApp(
  state = { videos: [], search: { videos: [], term: "" } },
  action
) {
  switch (action.type) {
    case "FILTERED_VIDEOS": {
      const newData = state.videos.filter(function(item) {
        const itemData = item.title
          ? item.title.toUpperCase()
          : "".toUpperCase();
        const textData = action.data.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });

      const videos = action.data ? newData : [];

      return { ...state, search: { term: action.data, videos } };
    }
    case "INIT_SEARCH": {
      return { ...state, search: { term: "", videos: [] } };
    }
    case "VIDEOS_SUCCESS": {
      const { data } = action;
      let videos = [];
      let questions = [];
      console.log("state", state, "data", data);
      data.map(item => {
        let persistedVideo =
          state.videos.find(video => video.id === item.id) || null;
        let bookMarked = persistedVideo ? persistedVideo.bookMarked : false;
        let completed = persistedVideo ? persistedVideo.completed : false;
        let results =
          persistedVideo &&
          persistedVideo.results &&
          persistedVideo.results.totalScore
            ? {
                totalScore: persistedVideo.results.totalScore,
                correctAnswers: [],
                incorrectAnswers: []
              }
            : {
                correctAnswers: [],
                incorrectAnswers: [],
                totalScore: 0
              };

        const {
          id,
          title: { rendered: title },
          content: { rendered: description },
          youtubeVideo,
          question1,
          question2,
          question3,
          question4,
          question5,
          question6,
          question7,
          question8,
          question9,
          question10,
          correctAnswer1,
          correctAnswer2,
          correctAnswer3,
          correctAnswer4,
          correctAnswer5,
          correctAnswer6,
          correctAnswer7,
          correctAnswer8,
          correctAnswer9,
          correctAnswer10,
          correctAnswerText1,
          correctAnswerText2,
          correctAnswerText3,
          correctAnswerText4,
          correctAnswerText5,
          correctAnswerText6,
          correctAnswerText7,
          correctAnswerText8,
          correctAnswerText9,
          correctAnswerText10,
          answers1,
          answers2,
          answers3,
          answers4,
          answers5,
          answers6,
          answers7,
          answers8,
          answers9,
          answers10,
          icon
        } = item;

        const newVideo = {
          id,
          title,
          preview: trimText(description, 100),
          description,
          youtubeVideo,
          questions: [
            {
              question: question1,
              correctAnswer: correctAnswer1,
              answers: answers1,
              correctAnswerText: correctAnswerText1
            },
            {
              question: question2,
              correctAnswer: correctAnswer2,
              answers: answers2,
              correctAnswerText: correctAnswerText2
            },
            {
              question: question3,
              correctAnswer: correctAnswer3,
              answers: answers3,
              correctAnswerText: correctAnswerText3
            },
            {
              question: question4,
              correctAnswer: correctAnswer4,
              answers: answers4,
              correctAnswerText: correctAnswerText4
            },
            {
              question: question5,
              correctAnswer: correctAnswer5,
              answers: answers5,
              correctAnswerText: correctAnswerText5
            },
            {
              question: question6,
              correctAnswer: correctAnswer6,
              answers: answers6,
              correctAnswerText: correctAnswerText6
            },
            {
              question: question7,
              correctAnswer: correctAnswer7,
              answers: answers7,
              correctAnswerText: correctAnswerText7
            },
            {
              question: question8,
              correctAnswer: correctAnswer8,
              answers: answers8,
              correctAnswerText: correctAnswerText8
            },
            {
              question: question9,
              correctAnswer: correctAnswer9,
              answers: answers9,
              correctAnswerText: correctAnswerText9
            },
            {
              question: question10,
              correctAnswer: correctAnswer10,
              answers: answers10,
              correctAnswerText: correctAnswerText10
            }
          ],
          current: 0,
          results,
          completed,
          icon,
          bookMarked
        };
        console.log(newVideo);
        videos.push(newVideo);
      });
      console.log(videos, state);
      return { ...state, videos: videos };
    }
    case "SUBMIT_ANSWER": {
      const { current, results, completed, id } = action.data;

      const newVideos = [];

      return {
        videos: state.videos.map(video =>
          video.id === id ? { ...video, current, results, completed } : video
        ),
        search: { term: "", videos: [] }
      };
    }

    case "NEXT_QUESTION": {
      const { id, current } = action.data;
      console.log("action", action);
      return {
        videos: state.videos.map(video =>
          video.id === id ? { ...video, current: current + 1 } : video
        ),
        search: { term: "", videos: [] }
      };
    }

    case "RESET_QUIZ": {
      const { id } = action;

      return {
        videos: state.videos.map(video =>
          video.id === id
            ? {
                ...video,
                completed: false,
                current: 0,
                results: {
                  correctAnswers: [],
                  incorrectAnswers: [],
                  totalScore: 0
                }
              }
            : video
        ),
        search: { term: "", videos: [] }
      };
    }
    case "BOOKMARK_VIDEO":
      const newVideos = [];
      state.videos.map(item => {
        const { id, bookMarked } = item;
        const newBookmark = id == action.video ? !bookMarked : bookMarked;
        const newItem = {
          ...item,
          bookMarked: newBookmark
        };
        newVideos.push(newItem);
      });
      return { videos: newVideos, search: { term: "", videos: [] } };

    default:
      return state;
  }
}
