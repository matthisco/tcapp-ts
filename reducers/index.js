import { combineReducers } from "redux";
import { tcApp } from "./videos";

const rootReducer = combineReducers({
	tcApp
});

export default rootReducer;
