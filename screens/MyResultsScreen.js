import React from "react";
import { connect } from "react-redux";
import { ScrollView, StyleSheet, View, Text } from "react-native";

class MyResultsScreen extends React.Component {
  render() {
    const { videos } = this.props;

    return (
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center"
        }}
      >
        <ScrollView contentContainerStyle={styles.scrollContainer}>
          {videos.map((item, key) =>
            item.results.totalScore != 0 ? (
              <View style={styles.episode} key={key}>
                <View style={styles.header}>
                  <Text style={styles.text}>{item.title}</Text>
                  <Text style={styles.result}>{item.results.totalScore} %</Text>
                </View>
              </View>
            ) : null
          )}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    videos: state.tcApp.videos
  };
};

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps)(MyResultsScreen);

const styles = StyleSheet.create({
  scrollContainer: {
    paddingVertical: 20,
    width: "100%",
    alignItems: "center",
    paddingTop: 15,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center"
  },
  container: {
    flex: 1
  },
  episode: {
    flexDirection: "column",
    width: 170,
    height: 150,
    margin: 3,
    opacity: 0.8,
    marginTop: 2,
    padding: 5,
    color: "white",
    backgroundColor: "#003333"
  },
  text: {
    color: "white"
  },
  result: {
    fontSize: 20,
    color: "white"
  },
  header: {
    color: "white",
    backgroundColor: "transparent",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  }
});
