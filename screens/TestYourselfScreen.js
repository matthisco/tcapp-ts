import React from "react";
import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  Picker,
  Button,
  Dimensions
} from "react-native";
import { connect } from "react-redux";
import { AnimatedCircularProgress } from "react-native-circular-progress";
import FadeInView from "react-native-fade-in-view";
import Question from "../components/Question";
import NavigationService from "../navigation/NavigationService";
import { submitAnswer, resetQuiz, nextQuestion } from "../actions/videos";
class TestYourselfScreen extends React.Component {
  constructor(props) {
    super(props);
  }

  reset = id => {
    const { resetQuiz } = this.props;

    resetQuiz(id);
  };

  next = (id, current) => {
    const { nextQuestion } = this.props;

    nextQuestion({ id, current });
  };

  submitAnswer = (index, answer, id) => {
    let results = {};
    const { submitAnswer } = this.props;
    let video = this.props.videos.find(obj => obj.id == id);

    let question = video.questions[index];

    let answers = question.answers.split("|");

    let isCorrect = answers[question.correctAnswer].trim() == answer.trim();

    let totalScore = isCorrect
      ? video.results.totalScore + 5
      : video.results.totalScore;

    let correctAnswers = video.results.correctAnswers;
    if (isCorrect) {
      correctAnswers.push(index);
    }
    let incorrectAnswers = video.results.incorrectAnswers;
    if (!isCorrect) {
      incorrectAnswers.push(index);
    }

    let currentResult = {
      correctAnswers,
      incorrectAnswers,
      totalScore
    };
    submitAnswer({
      id,
      current: index,
      results: currentResult,
      completed: index === 9 ? true : false
    });
  };

  componentDidMount() {
    // this.fetchQuestions();
  }

  render() {
    const screenWidth = Dimensions.get("window").width;
    const { id } = NavigationService.getParams();
    const { videos } = this.props;
    let video = videos.find(obj => obj.id == id);
    const { questions, completed, current, results } = video;
    let chartData = [];

    chartData.push(results.totalScore);
    const chartConfig = {
      backgroundGradientFrom: "#1E2923",
      backgroundGradientTo: "#08130D",
      strokeWidth: 2 // optional, default 3
    };

    return (
      <View style={styles.container}>
        {questions.length > 0 && completed === false && (
          <View>
            <Question
              onSelect={answer => {
                this.submitAnswer(current, answer, id);
              }}
              question={questions[current]}
              results={results}
              current={current}
            />
            {!results.correctAnswers.includes(current) &&
              results.incorrectAnswers.includes(current) && (
                <FadeInView duration={750} style={{ alignItems: "center" }}>
                  <Text style={styles.text}>
                    {questions[current].correctAnswerText}
                  </Text>
                </FadeInView>
              )}

            <Button
              title="Next"
              style={styles.button}
              disabled={
                !results.correctAnswers.includes(current) &&
                !results.incorrectAnswers.includes(current)
                  ? true
                  : false
              }
              onPress={() => this.next(id, current)}
            />
          </View>
        )}

        <View style={styles.container}>
          {completed === true && (
            <View style={styles.container}>
              <Text style={styles.textResult}>You scored:</Text>

              <AnimatedCircularProgress
                size={200}
                style={styles.chart}
                width={10}
                fill={results.totalScore}
                tintColor="#00e0ff"
                backgroundColor="#3d5875"
              >
                {fill => (
                  <Text style={styles.score}>{results.totalScore} %</Text>
                )}
              </AnimatedCircularProgress>

              <Button
                title="Restart Quiz"
                style={styles.button}
                onPress={() => this.reset(id)}
              />
            </View>
          )}
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    videos: state.tcApp.videos
  };
};
const mapDispatchToProps = dispatch => ({
  submitAnswer: data => dispatch(submitAnswer(data)),
  resetQuiz: id => dispatch(resetQuiz(id)),
  nextQuestion: data => dispatch(nextQuestion(data))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestYourselfScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  chart: {
    margin: 20
  },
  loadingQuestions: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  score: {
    color: "white",
    fontSize: 40
  },
  textResult: {
    color: "white",
    fontSize: 20,
    padding: 5,
    margin: 5
  },
  text: {
    color: "white",
    fontSize: 12,
    padding: 5,
    margin: 5,
    borderRadius: 3,
    backgroundColor: "#B20000"
  },
  button: {
    margin: 20,
    padding: 20
  }
});
