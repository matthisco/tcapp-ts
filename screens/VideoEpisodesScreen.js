import React, { Component } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  Image,
  View,
  ActivityIndicator
} from "react-native";
import { BackHandler } from "react-native";
import { connect } from "react-redux";

import { StackActions, NavigationActions } from "react-navigation";
import VideoList from "../components/VideoList";
// import { goBack } from "../navigation/actions.js";
import NavigationService from "../navigation/NavigationService.js";
class VideoEpisodesScreen extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }

  onBackPress = () => {
    const { dispatch } = this.props;

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Home" })]
    });
    this.props.navigation.dispatch(resetAction);
  };

  render() {
    const {
      videos,
      search: { videos: searchedVideos }
    } = this.props;

    let videoList = searchedVideos.length > 0 ? searchedVideos : videos;

    if (videos == "") {
      return (
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrollContainer}>
          <VideoList videos={videoList} {...this.props.navigation} />
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    videos: state.tcApp.videos,
    search: state.tcApp.search
  };
};

const mapDispatchToProps = dispatch => ({
  fetchVideos: () => dispatch(fetchVideos())
});

export default connect(mapStateToProps)(VideoEpisodesScreen);

const styles = StyleSheet.create({
  scrollContainer: {
    paddingVertical: 20,
    width: "100%",
    alignItems: "center",
    paddingTop: 15,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center"
  },
  container: {
    flex: 1
  }
});
