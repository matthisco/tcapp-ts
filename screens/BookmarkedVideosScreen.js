import React from "react";
import { connect } from "react-redux";
import { ScrollView, StyleSheet, View } from "react-native";
import VideoList from "../components/VideoList";
class BookmarkedVideosScreen extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const { videos } = this.props;
		const videoList = videos.filter(video => video.bookMarked == true);
		if (videos == "") {
			return (
				<View style={[styles.container, styles.horizontal]}>
					<ActivityIndicator size="large" color="#fff" />
				</View>
			);
		}

		return (
			<View style={styles.container}>
				<ScrollView contentContainerStyle={styles.scrollContainer}>
					<VideoList videos={videoList} {...this.props.navigation} />
				</ScrollView>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		videos: state.tcApp.videos
	};
};

const mapDispatchToProps = dispatch => ({
	fetchVideos: () => dispatch(fetchVideos()),
	loadState: () => dispatch(loadState())
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(BookmarkedVideosScreen);

const styles = StyleSheet.create({
	scrollContainer: {
		paddingVertical: 20,
		width: "100%",
		alignItems: "center",
		paddingTop: 15,
		flexDirection: "row",
		flexWrap: "wrap",
		justifyContent: "center"
	},
	container: {
		flex: 1
	}
});
