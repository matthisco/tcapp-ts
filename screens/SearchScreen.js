import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";

import VideoEpisode from "../components/VideoEpisode";
import { filteredVideo } from "../actions/videos";
class SearchScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>

      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  filteredVideo: () => dispatch(filteredVideo())
});

const mapStateToProps = state => {
  return {
    videos: state.videos
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: "transparent"
  }
});
