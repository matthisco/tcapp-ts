import React, { Component } from "react";
import { ScrollView, StyleSheet, Text, View, ActivityIndicator } from "react-native";
import { WebView } from "react-native-webview";

export default class AboutScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  componentDidMount() {
    fetch("http://app.rguc.co.uk/?rest_route=/wp/v2/pages/2")
      .then(response => response.json())
      .then(responseJson => {
        this.setState({ content: responseJson.content.rendered });
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    const { content } = this.state;
    console.log(this.props, content);

    if (content == "") {
      return (
        <View style={[styles.container, styles.horizontal]}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <WebView
          style={styles.webView}
          originWhitelist={["*"]}
          source={{ html: '<div style="color:white;">' + content + "</div>" }}
          scalesPageToFit={false}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent",
    color: "white",
    paddingTop: 15
  },
  webView: {
    backgroundColor: "transparent",
    color: "#fff"
  }
});
