import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  Image,
  View,
  WebView
} from "react-native";
import { AndroidBackHandler } from "react-navigation-backhandler";
import { connect } from "react-redux";

import { NavigationActions, StackActions } from "react-navigation";
import StyledText from "../components/StyledText";
import VideoPlayerHeader from "../components/VideoPlayerHeader";
import ShareComponent from "../components/Share";
import VideoPlayer from "../components/VideoPlayer";
import NavigationService from "../navigation/NavigationService";
import { bookmarkVideo } from "../actions/videos";

class VideoPlayerScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.bookmarkVideo = this.bookmarkVideo.bind(this);
  }

  onBackButtonPressAndroid = () => {
    /*
     *   Returning `true` from `onBackButtonPressAndroid` denotes that we have handled the event,
     *   and react-navigation's lister will not get called, thus not popping the screen.
     *
     *   Returning `false` will cause the event to bubble up and react-navigation's listener will pop the screen.
     * */

    const { dispatch } = this.props.navigation;

    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: "Drawer" })]
    });
    this.props.navigation.dispatch(resetAction);
    return true;
  };

  bookmarkVideo() {
    const {
      bookmarkVideo,
      navigation: { navigate }
    } = this.props;
    const { id } = NavigationService.getParams();
    bookmarkVideo(id);
    navigate("Video Player");
  }

  render() {
    const { id } = NavigationService.getParams();
    const { videos } = this.props;
    let videoProps = videos.find(obj => obj.id == id);

    let { description, youtubeVideo, title } = videoProps;

    return (
      <AndroidBackHandler onBackPress={this.onBackButtonPressAndroid}>
        <View style={styles.container}>
          <ScrollView>
            <VideoPlayerHeader {...videoProps} onClick={this.bookmarkVideo} />
            <VideoPlayer {...videoProps} />
            <StyledText text="Episode Overview" />
            <Text style={[styles.description, styles.text]}>{description}</Text>

            <View style={styles.section}>
              <View style={styles.leftContainer}>
                <Image
                  style={{
                    width: 40,
                    height: 30
                  }}
                  source={require("../assets/images/test-yourself.png")}
                />
              </View>
              <View style={styles.rightContainer}>
                <TouchableHighlight
                  onPress={() => {
                    const resetAction = StackActions.reset({
                      index: 0,
                      actions: [
                        NavigationActions.navigate({
                          routeName: "Drawer",
                          params: { id }
                        })
                      ]
                    });
                    const navigateAction = NavigationActions.navigate({
                      routeName: "TestYourself",
                      params: { id }
                    });
                    this.props.navigation.dispatch(resetAction);
                    this.props.navigation.dispatch(navigateAction);
                  }}
                >
                  <StyledText text="Test Yourself" />
                </TouchableHighlight>
              </View>
            </View>

            <View style={styles.section}>
              <View style={styles.leftContainer}>
                <ShareComponent {...videoProps} />
              </View>
              <View style={styles.rightContainer}>
                <StyledText text="Share This" />
              </View>
            </View>
          </ScrollView>
        </View>
      </AndroidBackHandler>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  bookmarkVideo: id => dispatch(bookmarkVideo(id))
});

const mapStateToProps = state => {
  return {
    videos: state.tcApp.videos
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoPlayerScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },

  text: { color: "white" },
  description: { padding: 5 },
  section: { flexDirection: "row", margin: 5 },
  leftContainer: {
    height: 30,
    width: 40,
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  rightContainer: {
    flexDirection: "row"
  }
});
