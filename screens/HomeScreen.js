import React from "react";
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
  View
} from "react-native";
import { connect } from "react-redux";
import { fetchData, loadState } from "../actions/videos";
import ResponsiveImage from "react-native-responsive-image";
import HomeMenu from "../components/HomeMenu";
import BasicHeader from "../navigation/BasicHeader";
import DrawerHeader from "../navigation/DrawerHeader";
class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    const { fetchData } = this.props;
    fetchData();
  }

  render() {
    const { videos } = this.props;

    if (videos === undefined || videos.length == 0) {
      return (
        <View style={{ flex: 1 }}>
          <ActivityIndicator size="large" color="#fff" />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <HomeMenu {...this.props} />
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  fetchData: () => dispatch(fetchData())
});
const mapStateToProps = state => {
  return {
    videos: state.tcApp.videos
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "transparent"
  }
});
