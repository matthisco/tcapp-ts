import React, { Component } from "react";
import { View, Text, Image, StyleSheet, Platform } from "react-native";
import { StackActions, NavigationActions } from "react-navigation";
import CustomImage from "./CustomImage";
import { images } from "../helpers/images";
export default class VideoEpisode extends React.Component {
  constructor(props) {
    super(props);
    this.state = { height: 215 };
  }

  render() {
    const { id, title, description, icon, video, preview, push, dispatch } = this.props;

    let imageSource = images[icon].uri;

    return (
      <View style={styles.episode}>
        <View style={styles.header}>
          <View style={styles.iconStyle}>
            {icon && (
              <Image
                style={styles.iconStyle}
                resizeMode="contain"
                source={imageSource}
              />
            )}
          </View>
          <View style={styles.title}>
            <Text
              onPress={() => {
                const resetAction = StackActions.reset({
                  index: 0,
                  actions: [
                    NavigationActions.navigate({
                      routeName: "VideoPlayer",
                      params: { id }
                    })
                  ]
                });
                dispatch(resetAction);
              }}
              style={styles.text}
            >
              {title}
            </Text>
          </View>
        </View>
        <View style={styles.content}>
          <Text style={styles.text}>{preview}</Text>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  episode: {
    flexDirection: "column",
    width: 170,
    height: 150,
    margin: 3,
    opacity: 0.8,
    marginTop: 2,
    padding: 5,
    color: "white",
    backgroundColor: "#003333"
  },
  header: {
    color: "white",
    backgroundColor: "transparent",
    flexDirection: "row"
  },
  icon: {},
  iconStyle: { width: 32, height: 40 },
  title: {
    color: "white",
    flex: 1,
    textAlign: "center",
    flexWrap: "wrap",
    marginLeft: 5
  },
  content: {
    alignItems: "center"
  },
  text: {
    color: "white"
  }
});
