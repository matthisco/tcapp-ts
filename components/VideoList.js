import React, { Component } from "react";
import { View, Text, Image, StyleSheet, Platform } from "react-native";

import VideoEpisode from "./VideoEpisode";

import { images } from "../helpers/images";
export default class VideoList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { videos } = this.props;

    return videos.map((item, key) => (
      <VideoEpisode
        icon={item.icon}
        key={key}
        id={item.id}
        description={item.description}
        preview={item.preview}
        title={item.title}
        video={item.youtubeVideo}
        {...this.props}
      />
    ));
  }
}
const styles = StyleSheet.create({
  episode: {
    flexDirection: "column",
    width: 170,
    height: 150,
    margin: 3,
    opacity: 0.8,
    marginTop: 2,
    padding: 5,
    color: "white",
    backgroundColor: "#003333"
  },
  header: {
    color: "white",
    backgroundColor: "transparent",
    flexDirection: "row"
  },
  icon: {},
  iconStyle: { width: 32, height: 40 },
  title: {
    color: "white",
    flex: 1,
    textAlign: "center",
    flexWrap: "wrap",
    marginLeft: 5
  },
  content: {
    alignItems: "center"
  },
  text: {
    color: "white"
  }
});
