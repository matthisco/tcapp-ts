import React, { Component } from "react";
import {
    Dimensions,
    AppRegistry,
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Button,
    StatusBar
} from "react-native";
import { connect } from "react-redux";
import StyledText from "../components/StyledText";
import SearchBar from "react-native-searchbar";
const DEVICE_WIDTH = Dimensions.get(`window`).width;

class Search extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.searchBar.hide();
    }
    filteredVideo = filteredData => {
        const { filterVideo } = this.props;
        console.log("filterVideo", this.props);
        filterVideo(filteredData);
    };

    //     filteredVideo = filteredData => {
    //         const { filterVideo } = this.props;
    //
    //         filterVideo(filteredData);
    //     };
    _handleResults(results) {}
    filterList = event => {
        const { filteredVideo } = this.props;
        console.log("filterVideoCallback", this.props);
        //         let videos = this.state.initialVideos;
        //
        //         let {
        //             nativeEvent: { text }
        //         } = event;
        //         let filteredData = videos.filter(item =>
        //             item.title.toLowerCase().includes(text.toLowerCase())
        //         );
        //
        //         this.filteredVideo(filteredData);
    };
    render() {
        const { videos } = this.props;
        console.log("props", this.props);
        return (
            <View style={styles.container}>
                <View style={styles.container}>
                    <View style={{ paddingLeft: 5 }}>
                        <StyledText text={title} />
                    </View>
                    <View style={{ justifyContent: "flex-end" }}>
                        <Image
                            source={require("../assets/images/tc-logo.png")}
                            style={{ width: 120, height: 40, marginRight: 5 }}
                        />
                        <TouchableHighlight
                            style={{
                                width: 32,
                                marginLeft: 10,
                                marginTop: 10,
                                marginRight: 5,
                                alignSelf: "flex-end"
                            }}
                            onPress={() => {
                                this.props.navigation.openDrawer();
                            }}
                        >
                            <Image
                                style={{
                                    width: 32,
                                    height: 25,
                                    margin: 3
                                }}
                                source={require("../assets/images/menu-icon.png")}
                            />
                        </TouchableHighlight>
                    </View>
                </View>

                <TouchableOpacity onPress={() => this.searchBar.show()}>
                    <View
                        style={{
                            backgroundColor: "green",
                            height: 100,
                            width: 100,
                            marginTop: 50
                        }}
                    />
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.searchBar.hide()}>
                    <View
                        style={{
                            backgroundColor: "red",
                            height: 100,
                            width: 100
                        }}
                    />
                </TouchableOpacity>

                <SearchBar
                    ref={ref => (this.searchBar = ref)}
                    data={videos}
                    handleResults={this._handleResults}
                    showOnLoad
                />
            </View>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    filteredVideo: () => dispatch(filteredVideo())
});

const mapStateToProps = state => {
    return {
        videos: state.videos
    };
};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Search);

const styles = StyleSheet.create({
    text: {},
    container: {
        height: 100,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row",
        backgroundColor: "#f5fcff"
    },
    status: {
        zIndex: 10,
        elevation: 2,
        width: DEVICE_WIDTH,
        height: 21,
        backgroundColor: "#0097a7"
    },
    header: {
        justifyContent: "center",
        alignItems: "center",
        width: DEVICE_WIDTH,
        height: 56,
        marginBottom: 6,
        backgroundColor: "#00bcd4"
    },
    label: {
        flexGrow: 1,
        fontSize: 20,
        fontWeight: `600`,
        textAlign: `left`,
        marginVertical: 8,
        paddingVertical: 3,
        color: `#f5fcff`,
        backgroundColor: `transparent`
    },
    button: {
        justifyContent: "center",
        alignItems: "center",
        width: 130,
        height: 40,
        marginTop: 40,
        borderRadius: 2,
        backgroundColor: `#ff5722`
    }
});
