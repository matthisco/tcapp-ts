import React, { Component } from "react";
import PropTypes from "prop-types";

import { StyleSheet, Text, View } from "react-native";

const StyledText = props => {
	let { text } = props;
	let firstWord = text.replace(/ .*/, "").toUpperCase();
	let restOfTitle = text.substr(text.indexOf(" ") + 1).toUpperCase();

	return (
		<View style={styles.container}>
			<Text style={styles.title}>
				<Text style={{ fontFamily: "Lato-Light" }}>
					{firstWord + " "}
				</Text>
				<Text style={{ fontFamily: "Lato-Bold" }}>{restOfTitle}</Text>
			</Text>
		</View>
	);
};

export default StyledText;

const styles = StyleSheet.create({
	title: {
		color: "#fff",
		fontSize: 18
	},
	container: {
		textAlign: "left",
		marginLeft: 5
	}
});
