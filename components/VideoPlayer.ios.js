import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  AppState,
  Dimensions,
  Platform,
  StatusBar
} from "react-native";
import YouTube, {
  YouTubeStandaloneIOS,
  YouTubeStandaloneAndroid
} from "react-native-youtube";
import StyledText from "./StyledText";
import VideoPlayerHeader from "./VideoPlayerHeader";
import ShareComponent from "./Share";

import { WebView } from "react-native-webview";
export default class VideoPlayer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState
    };
  }
  componentDidMount() {
    AppState.addEventListener("change", this._handleAppStateChange);
  }
  componentWillUnmount() {
    AppState.removeEventListener("change", this._handleAppStateChange);
  }

  _handleAppStateChange = nextAppState => {
    this.setState({ appState: nextAppState });
  };

  render() {
    let {
      id,
      icon,
      title,
      bookMarked,
      description,
      youtubeVideo,
      preview,
      url
    } = this.props;
    console.log(this.props);

    return (
      <View style={styles.container}>
        <WebView
          javaScriptEnabled={true}
          source={{
            uri: `https://www.youtube.com/embed/${youtubeVideo}`
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "pink",
    height: 250,
    width: "100%"
  }
});
