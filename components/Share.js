import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Clipboard,
  ToastAndroid,
  AlertIOS,
  Image,
  Platform
} from "react-native";
import Share, { ShareSheet, Button } from "react-native-share";

export default class ShareComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false
    };
  }
  onCancel() {
    console.log("CANCEL");
    this.setState({ visible: false });
  }
  onOpen() {
    console.log("OPEN");
    this.setState({ visible: true });
  }
  render() {
    const { id, title, preview, youtubeVideo } = this.props;

    const url = `https://www.youtube.com/watch?v=${youtubeVideo}`;

    let shareOptions = {
      title,
      preview,
      url,
      subject: "Share Link" //  for email
    };

    let shareImageBase64 = {
      title,
      preview,
      url,
      subject: "Share Link" //  for email
    };

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => {
            Share.open(shareOptions);
          }}
        >
          <View style={styles.instructions}>
            <Image
              style={{
                width: 32,
                height: 32
              }}
              source={require(`../assets/images/share-icon.png`)}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 30,
    width: 30,
    justifyContent: "center",
    alignItems: "center"
  },
  instructions: {
    marginTop: 20,
    marginBottom: 20
  }
});
