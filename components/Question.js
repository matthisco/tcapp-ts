import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Button,
  Dimensions,
  TouchableHighlight
} from "react-native";

export default class Question extends React.Component {
  constructor() {
    super();
  }

  _onPressButton = (index, answer, id) => {
    const { onSelect } = this.props;
    onSelect(index, answer, id);
  };

  renderOptions = question => {
    const { id, current, results } = this.props;
    const result = [];

    let answers = question.answers.split("|");

    answers.forEach((item, index) => {
      let answerColor = question.correctAnswer == index ? "green" : "#B20000";
      let answeredColor =
        results.correctAnswers.includes(current) ||
        results.incorrectAnswers.includes(current)
          ? answerColor
          : "#808080";
      result.push(
        <TouchableHighlight
          key={index}
          style={[styles.radioText, { backgroundColor: answeredColor }]}
          underlayColor={answeredColor || answerColor}
          onPress={() => {
            !results.correctAnswers.includes(current) &&
            !results.incorrectAnswers.includes(current)
              ? this._onPressButton(item, index, id)
              : null;
          }}
        >
          <Text style={styles.button}>{item.trim()}</Text>
        </TouchableHighlight>
      );
    });

    return result;
  };

  render() {
    const { question, answers } = this.props;

    return (
      <View
        style={{
          flex: 1
        }}
      >
        <Text style={{ fontSize: 16, color: "#fff", textAlign: "right" }}>
          {this.props.current + 1} out of 10
        </Text>
        <Text style={{ fontSize: 20, fontWeight: "bold", color: "#fff" }}>
          {this.props.question.question}
        </Text>

        {this.renderOptions(question)}
      </View>
    );
  }
}

const getWidth = () => {
  const screenWidth = Dimensions.get("window").width - 20;

  return screenWidth;
};

const styles = StyleSheet.create({
  radioText: {
    width: getWidth(),

    margin: 3,
    padding: 5,
    borderRadius: 3
  },
  button: {
    color: "white",
    fontSize: 16
  }
});
