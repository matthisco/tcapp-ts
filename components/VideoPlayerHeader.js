import React, { Component } from "react";
import PropTypes from "prop-types";

import {
	StyleSheet,
	Text,
	View,
	Image,
	TouchableHighlight
} from "react-native";
import StyledText from "./StyledText";
import { images } from "../helpers/images";
export default class VideoPlayerHeader extends React.Component {
	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		let { onClick, id } = this.props;

		onClick(id);
	}

	render() {
		let { title, bookMarked, icon, id } = this.props;

		let imageSource = images[icon].uri;
		return (
			<View style={styles.container}>
				<View style={styles.navBar}>
					<View style={styles.leftContainer}>
						<Image
							style={styles.iconStyle}
							resizeMode="contain"
							source={imageSource}
						/>
					</View>
					<View style={styles.title}>
						<StyledText text={title} />
					</View>
					<View style={styles.rightContainer}>
						<TouchableHighlight onPress={this.handleClick}>
							{bookMarked ? (
								<Image
									style={{
										width: 25,
										height: 32
									}}
									source={require("../assets/images/bookmark-filled.png")}
								/>
							) : (
								<Image
									style={{
										width: 25,
										height: 32
									}}
									source={require("../assets/images/bookmark.png")}
								/>
							)}
						</TouchableHighlight>
					</View>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: { height: 70, width: "100%" },
	iconStyle: { width: 50, height: 40 },
	navBar: {
		height: 60,
		width: "100%",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center"
	},
	leftContainer: {
		height: 30,
		width: 30,
		flexDirection: "row",
		justifyContent: "flex-start"
	},
	rightContainer: {
		flexDirection: "row",
		justifyContent: "flex-end",
		alignItems: "center"
	}
});
