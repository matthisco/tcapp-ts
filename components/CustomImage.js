import React, { Component } from "react";
import PropTypes from "prop-types";

import { StyleSheet, Text, View, Image } from "react-native";

const CustomImage = props => {
	console.log(props);
	// if (!this.props.source) return null;

	return (
		<View>
			<Image
				style={{ width: 250, height: 250 }}
				resizeMode="contain"
				// source={require('./assets/rn-school-logo.png')}
				source={props.source}
			/>
		</View>
	);
};

export default CustomImage;
