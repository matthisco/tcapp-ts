import React, { Component } from "react";
import {
  BackHandler,
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  AppState,
  Dimensions,
  Platform,
  StatusBar
} from "react-native";
import YouTube, {
  YouTubeStandaloneIOS,
  YouTubeStandaloneAndroid
} from "react-native-youtube";
import StyledText from "./StyledText";
import VideoPlayerHeader from "./VideoPlayerHeader";
import ShareComponent from "./Share";
import { WebView } from "react-native-webview";

export default class VideoPlayer extends React.PureComponent {
  constructor(props) {
    super(props);
  }

//   componentDidMount() {
//     this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
//       return this.props.navigation.navigate("Dashboard");
//     });
//   }
// 
//   componentWillUnmount() {
//     this.backHandler.remove();
//   }

  //   componentDidMount() {
  //     this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  //   }
  //
  //   componentWillUnmount() {
  //     this.backHandler.remove()
  //   }

  //   componentWillUnmount() {
  //      this.BackHandler.remove()
  //   }
  //
  //   componentDidMount() {
  //     BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  //   }

//   onBackPress = () => {
//     const { dispatch } = this.props;
// 
//     const resetAction = StackActions.reset({
//       index: 0,
//       actions: [NavigationActions.navigate({ routeName: "Home" })]
//     });
//     this.props.navigation.dispatch(resetAction);
//   };

  render() {
    let {
      id,
      icon,
      title,
      bookMarked,
      description,
      youtubeVideo,
      preview,
      url
    } = this.props;

    return (
      <View style={styles.container}>
        <WebView
          mediaPlaybackRequiresUserAction={true}
          style={{
            height: 240,
            width: "100%",
            alignSelf: "center",
            alignContent: "center"
          }}
          source={{
            uri: `https://www.youtube.com/embed/${youtubeVideo}?rel=0`
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "black",
    height: 250,
    width: "100%"
  }
});
