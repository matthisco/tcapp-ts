import React from "react";
import { View, Text, Image, StyleSheet, Platform } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

export default class HomeMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            marginTop: 2,
            padding: 5,
            alignItems: "center",
            width: "100%",
            opacity: 0.8,
            backgroundColor: "#003366"
          }}
        >
          <View
            style={{
              width: 225,
              alignItems: "center",
              justifyContent: "flex-start",
              flexDirection: "row"
            }}
          >
            <Image
              source={require("../assets/images/video-episodes.png")}
              style={styles.icon}
            />

            <Text
              style={styles.menuText}
              onPress={() => {
                this.props.navigation.navigate("VideoEpisodes");
              }}
            >
              VIDEO EPISODES
            </Text>
          </View>
        </View>

        <View
          style={{
            marginTop: 2,
            padding: 5,
            alignItems: "center",
            width: "100%",
            opacity: 0.8,
            backgroundColor: "#0D4C75"
          }}
        >
          <View
            style={{
              width: 225,
              alignItems: "center",
              justifyContent: "flex-start",
              flexDirection: "row"
            }}
          >
            <Image
              source={require("../assets/images/test-yourself.png")}
              style={styles.icon}
            />
            <Text
              style={styles.menuText}
              onPress={() => {
                this.props.navigation.navigate("TestYourself");
              }}
            >
              TEST YOURSELF
            </Text>
          </View>
        </View>
        <View
          style={{
            marginTop: 2,
            padding: 5,
            alignItems: "center",
            width: "100%",
            opacity: 0.8,
            backgroundColor: "#1180A6"
          }}
        >
          <View
            style={{
              width: 225,

              alignItems: "center",
              justifyContent: "flex-start",
              flexDirection: "row"
            }}
          >
            <Image
              source={require("../assets/images/my-results.png")}
              style={styles.icon}
            />
            <Text
              style={styles.menuText}
              onPress={() => {
                this.props.navigation.navigate("MyResults");
              }}
            >
              MY RESULTS
            </Text>
          </View>
        </View>
        <View
          style={{
            marginTop: 2,
            padding: 5,
            alignItems: "center",
            width: "100%",
            opacity: 0.8,
            backgroundColor: "#0CA8A8"
          }}
        >
          <View
            style={{
              width: 225,

              alignItems: "center",
              justifyContent: "center",
              flexDirection: "row"
            }}
          >
            <Image
              source={require("../assets/images/bookmarked-videos.png")}
              style={styles.icon}
            />
            <Text
              style={styles.menuText}
              onPress={() => {
                this.props.navigation.navigate("BookmarkedVideos");
              }}
            >
              BOOKMARKED VIDEOS
            </Text>
          </View>
        </View>

        <View
          style={{
            marginTop: 2,
            padding: 5,
            alignItems: "center",
            width: "100%",
            opacity: 0.8,
            backgroundColor: "#006666"
          }}
        >
          <View
            style={{
              width: 225,

              alignItems: "center",
              justifyContent: "flex-start",
              flexDirection: "row"
            }}
          >
            <Image
              style={styles.icon}
              source={require("../assets/images/search-icon.png")}
            />
            <Text
              style={styles.menuText}
              onPress={() => {
                this.props.navigation.navigate("Search");
              }}
            >
              SEARCH
            </Text>
          </View>
        </View>
        <View
          style={{
            marginTop: 2,
            padding: 5,
            alignItems: "center",
            width: "100%",
            opacity: 0.8,
            backgroundColor: "#003333"
          }}
        >
          <View
            style={{
              width: 225,
              alignItems: "center",
              justifyContent: "flex-start",
              flexDirection: "row"
            }}
          >
            <Image
              source={require("../assets/images/about.png")}
              style={styles.icon}
            />
            <Text
              style={styles.menuText}
              onPress={() => {
                this.props.navigation.navigate("About");
              }}
            >
              ABOUT
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    paddingTop: 20,
    backgroundColor: "transparent"
  },

  icon: {
    resizeMode: "center",
    width: 45,
    height: 45,
    opacity: 1,
    marginRight: 10,
    marginLeft: 20
  },

  menuText: {
    fontSize: 15,
    color: "#fff",
    opacity: 1,
    fontFamily: "lato-regular",
    textTransform: "uppercase"
  }
});
